package com.example.activitiesandlayouts

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity

class SplashActivity: AppCompatActivity() {

    private val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        handler.postDelayed({ routeToMainScreen() }, CHANGED_SCREEN_DELAY)
    }

    private fun routeToMainScreen() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private companion object {
        const val CHANGED_SCREEN_DELAY = 3000L
    }
}