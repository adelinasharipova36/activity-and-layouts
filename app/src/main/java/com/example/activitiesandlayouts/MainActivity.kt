package com.example.activitiesandlayouts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.activitiesandlayouts.AppConstants.EXTRA_AGE
import com.example.activitiesandlayouts.AppConstants.EXTRA_FAMILY_NAME
import com.example.activitiesandlayouts.AppConstants.EXTRA_HOBBIE
import com.example.activitiesandlayouts.AppConstants.EXTRA_NAME
import com.example.activitiesandlayouts.AppConstants.EXTRA_PATRONOMIC_NAME
import com.example.activitiesandlayouts.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnInfo.setOnClickListener { routeToInfoScreen() }
    }

    private fun routeToInfoScreen() {
        val intent = Intent(this, InfoActivity::class.java)
        with(binding) {
            intent.apply {
                putExtra(EXTRA_FAMILY_NAME, etFamilyName.text.toString())
                putExtra(EXTRA_NAME, etName.text.toString())
                putExtra(EXTRA_PATRONOMIC_NAME, etPatronomic.text.toString())
                putExtra(EXTRA_AGE, etAge.text.toString())
                putExtra(EXTRA_HOBBIE, etHobbies.text.toString())
            }
        }
        startActivity(intent)
    }
}