package com.example.activitiesandlayouts

object AppConstants {

    const val EXTRA_NAME = "name"
    const val EXTRA_FAMILY_NAME = "family_name"
    const val EXTRA_PATRONOMIC_NAME = "patronomic_name"
    const val EXTRA_AGE = "age"
    const val EXTRA_HOBBIE = "hobbie"
}