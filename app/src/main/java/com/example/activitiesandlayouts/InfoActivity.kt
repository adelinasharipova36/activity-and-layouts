package com.example.activitiesandlayouts

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.activitiesandlayouts.AppConstants.EXTRA_AGE
import com.example.activitiesandlayouts.AppConstants.EXTRA_FAMILY_NAME
import com.example.activitiesandlayouts.AppConstants.EXTRA_HOBBIE
import com.example.activitiesandlayouts.AppConstants.EXTRA_NAME
import com.example.activitiesandlayouts.AppConstants.EXTRA_PATRONOMIC_NAME
import com.example.activitiesandlayouts.databinding.ActivityInfoBinding


class InfoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityInfoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

            if (intent.getStringExtra(EXTRA_NAME) != null) binding.tvAnswerName.text = intent.getStringExtra(EXTRA_NAME)

            if (intent.getStringExtra(EXTRA_FAMILY_NAME) != null) binding.tvAnswerFamilyName.text = intent.getStringExtra(EXTRA_FAMILY_NAME)

            if (intent.getStringExtra(EXTRA_PATRONOMIC_NAME) != null) binding.tvAnswerPatronomicName.text = intent.getStringExtra(EXTRA_PATRONOMIC_NAME)

            if (intent.getStringExtra(EXTRA_AGE) != null) binding.tvAnswerAge.text = intent.getStringExtra(EXTRA_AGE)

            if (intent.getStringExtra(EXTRA_HOBBIE) != null) binding.tvAnswerHobbies.text = intent.getStringExtra(EXTRA_HOBBIE)

    }
}